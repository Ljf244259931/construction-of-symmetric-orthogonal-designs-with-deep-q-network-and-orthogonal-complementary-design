import numpy as np

def check_difficult_Orthogonal_experimental_design_judgement(matrix,q=3):

    row,column=np.shape(matrix)
    loss=0
    for i in range(column):
        for j in range(i+1,column):
            #print(sum(matrix[:,i]*matrix[:,j]))
            #if sum(matrix[:,i]*matrix[:,j])!=2:
            #if sum(matrix[:,i]*matrix[:,j])**2!=0
            loss+=check_two_column_orthogonal_judgement(matrix[:,i],matrix[:,j],q)#return False
            #print(loss)
            #print(sum(matrix[:,i]*matrix[:,j])**2)
    return loss

def check_two_column_orthogonal_judgement(a,b,q=3):
    if np.shape(a)==np.shape(b):
        state={}
        n=np.shape(a)[0]
        repeat_time=int(n/(q**2))
        for i in range(q):
            for j in range(q):
                state[(i,j)]=-repeat_time
        #print(repeat_time)
        for i in range(n):
            try:
                state[(a[i],b[i])]+=1
            except:
                state[(a[i],b[i])]=-repeat_time+1
        #print(state)
        loss=sum(np.array(list(state.values()))**2)
        
        return loss
    else:
        print("the shape of a,b are different")
        return 999


def difficult_Orthogonal_experimental_design_judgement(matrix,update_place,q):
    update_row,update_column=update_place
    row,column=np.shape(matrix)
    loss=1
    
    for i in range(update_column):
        loss=two_column_orthogonal_judgement(matrix[:,i],matrix[:,update_column],update_place,q=q)
        
        if loss==False:
#            print(i)
            return -1
    return 0

def random_matrix(n=27,s=4,q=3,sub_matrix=None):
    if not type(sub_matrix)==type(np.array([1])):
        matrix=np.zeros((n,s),dtype=int)
        repeat_column=int(n/q)
        repeat_time=int(repeat_column/q)
        #print(repeat_column)
        for i in range(q):
            for j in range(q):
                for h in range(repeat_time):
    
                    matrix[i*q*repeat_time+j*repeat_time+h][0],matrix[i*q*repeat_time+j*repeat_time+h][1]=i,j
        for j in range(2,s):
            for i in range(n):
                matrix[i][j]=-1
        return matrix
    else:
        r,c=np.shape(sub_matrix)
        matrix=np.zeros((n,s-c),dtype=int)
        for j in range(0,s-c):
            for i in range(n):
                matrix[i][j]=-1
        matrix=np.concatenate((sub_matrix,matrix),axis=1)
        return matrix

def two_column_orthogonal_judgement(a,b,update_place,q):
    update_row,update_column=update_place
    n=len(a)
    state={}
    for i in range(q):
        for j in range(q):
            state[(i,j)]=0
    if np.shape(a)==np.shape(b):
        
        repeat_time=int(n/(q**2))
        for i in range(update_row+1):
            if a[i]==-1 or b[i]==-1:
                continue
            state[(a[i],b[i])]+=1
        
            if max(list(state.values()))>repeat_time:
                return False

        return True
    else:
        print("the shape of a,b are different")
        return 999

class Solution(object):
    def __init__(self,n,s,q):
        self.n=n
        self.s=s
        self.q=q
        self.matrix=None
    def isValue(self,board,x,y):
        return difficult_Orthogonal_experimental_design_judgement(board,update_place=(x,y),q=self.q)
    
    def dfs(self,board):
        #遍历每一个坐标
        for j in range(5,self.s):
            for i in range(self.n):
            
                #找board里的需要填入的位置
                if board[i][j] == -1:
                    #从可选之间选一个
                    for k in [0,1,2]:
                        board[i][j] = k
                        #在if的时候调用递归
                        #如果这个递归可以返回true并且当前填入的数字也没毛病
                        #则证明我们解完了数独
                        if self.isValue(board,i,j) and self.dfs(board):
                            
                            return True
                        #到这个位置说明填入的数不太行，所以把它先空着。
                        board[i][j] = -1
                    #进行完当前可选的所有数字都不行，说明上一次决策有问题，返回false
                
                    return False
        #所有'.'都填满，并且没毛病，返回true
        print(board,check_difficult_Orthogonal_experimental_design_judgement(board,self.q))
        self.matrix=board
        return True
        
    def solveSudoku(self, board):
        """
        :type board: List[List[str]]
        :rtype: void Do not return anything, modify board in-place instead.
        """
        self.dfs(board)
if __name__ == '__main__':
    n=27
    s=13
    q=3
    ai=Solution(n=n,s=s,q=q)
    
    a=np.array([[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,1,1,1],[0,1,1,1],[0,1,1,1],[0,2,2,2],[0,2,2,2],[0,2,2,2],[1,0,1,2],[1,0,1,2],[1,0,1,2],[1,1,2,0],[1,1,2,0],[1,1,2,0],[1,2,0,1],[1,2,0,1],[1,2,0,1],[2,0,2,1],[2,0,2,1],[2,0,2,1],[2,1,0,2],[2,1,0,2],[2,1,0,2],[2,2,1,0],[2,2,1,0],[2,2,1,0]])
    matrix=random_matrix(n,s,q,sub_matrix=a)

    ai.solveSudoku(board=matrix)
    true_matrix = ai.matrix
