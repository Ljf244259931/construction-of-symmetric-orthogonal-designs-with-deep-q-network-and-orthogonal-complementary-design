import numpy as np


def sc(n, s, q, D, lb_coll):
    """
    sequential construction
    :param n: the number of runs
    :param s: the number of factors
    :param q: the number of levels
    :param k: the number of columns in D
    :param D: current design
    :param lb_coll: collection of level basis
    :return: n_cd, con, cc_coll
    """
    # reordering of current design
    D = D[np.lexsort((D[:, 1], D[:, 0])), :]
    # level basis of design
    A = np.concatenate((np.isin(D, 0), np.isin(D, 1)), axis=1).astype(np.int)
    # the number of columns
    k = np.size(D, 1)
    # frequency of level combination
    m = np.array(n/(q**2)).astype(np.int)
    # index of candidate basis
    cand_index = np.array(np.sum(np.dot(A.T, lb_coll)==m, axis=0) == np.size(A,1))
    # collection of candidate basis
    cb_coll = lb_coll[:, cand_index]
    n_cd, cand_domain = cd(n, s, q, k, cb_coll)
    con, cc_coll = cc(n, s, q, k, n_cd, cb_coll, cand_domain)
    return n_cd, con, cb_coll, cc_coll

def cd(n, s, q, k, cb_coll):
    """
    candidate domain
    :param n: the number of runs
    :param s: the number of factors
    :param q: the number of levels
    :param k: the number of columns in D
    :param cb_coll: collection of candidate basis
    :return: n_cd, cand_domain
    """
    # initialization of candidate domain
    cand_domain = np.empty([0, q])
    n_cb = np.size(cb_coll, 1)
    # size of candidate domain
    n_cd = 0
    if n_cb > 0:
        n_bin = np.ceil(n_cb / 10000).astype(np.int)
        for i in np.arange(n_bin):
            if i < n_bin - 1:
                for j in np.arange(10000) + i * 10000:
                    # table of candidate basis
                    cb_table = np.dot(cb_coll[:, j].reshape(1, n), cb_coll)
                    # index of complementary basis
                    comp_index = np.argwhere(cb_table == 0)[:, 1]
                    # collection of complementary basis
                    comp_coll = cb_coll[:, comp_index]
                    # table of complementary basis
                    comp_table = np.dot(comp_coll.T, comp_coll)
                    # combination of complementary basis
                    comp_comb = np.argwhere(comp_table == 0)
                    # index of candidate domain
                    cd_index1 = np.repeat(j, np.size(comp_comb, 0)).reshape(np.size(comp_comb, 0), 1)
                    cd_index2 = comp_index[comp_comb[:, 0]].reshape(np.size(comp_comb, 0), 1)
                    cd_index3 = comp_index[comp_comb[:, 1]].reshape(np.size(comp_comb, 0), 1)
                    # collection of candidate domain
                    cand_domain = np.concatenate(
                        (cand_domain, np.sort(np.concatenate((cd_index1, cd_index2, cd_index3), axis=1), axis=1)),
                        axis=0)
            else:
                for j in np.arange(i * 10000, n_cb):
                    # table of candidate basis
                    cb_table = np.dot(cb_coll[:, j].reshape(1, n), cb_coll)
                    # index of complementary basis
                    comp_index = np.argwhere(cb_table == 0)[:, 1]
                    # collection of complementary basis
                    comp_coll = cb_coll[:, comp_index]
                    # table of complementary basis
                    comp_table = np.dot(comp_coll.T, comp_coll)
                    # combination of complementary basis
                    comp_comb = np.argwhere(comp_table == 0)
                    # index of candidate domain
                    cd_index1 = np.repeat(j, np.size(comp_comb, 0)).reshape(np.size(comp_comb, 0), 1)
                    cd_index2 = comp_index[comp_comb[:, 0]].reshape(np.size(comp_comb, 0), 1)
                    cd_index3 = comp_index[comp_comb[:, 1]].reshape(np.size(comp_comb, 0), 1)
                    # collection of candidate domain
                    cand_domain = np.concatenate(
                        (cand_domain, np.sort(np.concatenate((cd_index1, cd_index2, cd_index3), axis=1), axis=1)),
                        axis=0)
            cand_domain = np.unique(cand_domain, axis=0).astype(np.int)
            n_cd = np.size(cand_domain, 0)
    return n_cd, cand_domain

def cc(n, s, q, k, n_cd, cb_coll, cand_domain):
    """
    candidate classification
    :param n: the number of runs
    :param s: the number of factors
    :param q: the number of levels
    :param k: the number of columns in D
    :param n_cd: size of candidate domain
    :param cb_coll: collection of candidate basis
    :param cand_domain: collection of candidate domain
    :return: con, cc_coll
    """
    # frequency of level combination
    m = np.array(n / (q ** 2)).astype(np.int)
    # collection of candidate columns
    cc_coll = np.empty([n, 0])
    # crossroad
    if n_cd > s-k:
        # a crossroad is reached without any one-way streets
        con = 0
        for i in np.arange(n_cd):
            # combination of candidate columns
            cc_comb = np.zeros([1, n_cd]).astype(np.int)
            for j in np.arange(n_cd):
                if i == j:
                    cc_comb[0, j] = 1
                elif np.sum(np.dot(cb_coll[:, cand_domain[i, :]].T, cb_coll[:, cand_domain[j, :]]) == m) == q**2:
                    cc_comb[0, j] = 1
            # size of candidate column
            cc_size = np.sum(cc_comb)
            if cc_size == s-k:
                cc_coll = dc(q, m, s-k, cb_coll, cc_coll, cand_domain[np.argwhere(cc_comb==1)[:, 1].reshape(-1), :])
                if np.size(cc_coll) > 0:
                    # a crossroad is reached with a one-way street
                    con = 1
                    break
    # one-way street
    elif n_cd == s-k:
        cc_coll = dc(q, m, n_cd, cb_coll, cc_coll, cand_domain)
        if np.size(cc_coll) > 0:
            # a one-way street is reached
            con = 2
        else:
            # a one-way street with a dead-end street is reached
            con = 3
    # dead-end street
    else:
        # a dead-end street is reached
        con = 4
    return con, cc_coll

def dc(q, m, n_cd, cb_coll, cc_coll, cand_domain):
    """
    direct construction
    :param q: the number of levels
    :param m: frequency of level combination
    :param n_cd: size of candidate domain
    :param cb_coll: collection of candidate basis
    :param cc_coll: collection of candidate columns
    :param cand_domain: collection of candidate domain
    :return: cc_coll
    """
    # index of candidate columns
    cc_index = cand_domain[:, 0:-1].reshape(-1)
    # table of candidate columns
    cc_table = np.dot(cb_coll[:, cc_index].T, cb_coll[:, cc_index])
    # attempt to construct the remaining columns directly
    if np.sum(cc_table == m) == np.size(cc_table) - ((q - 1) ** 2) * n_cd:
        for i in np.arange(n_cd):
            cc_coll = np.concatenate((cc_coll, np.dot(cb_coll[:, cand_domain[i, :]], np.arange(q).reshape(q, 1))), axis=1)
    return cc_coll