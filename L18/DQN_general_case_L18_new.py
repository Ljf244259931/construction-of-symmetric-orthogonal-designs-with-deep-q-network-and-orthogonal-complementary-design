﻿# -*- coding: utf-8 -*-
"""
Created on Sun Oct 27 15:23:27 2019

@author: jianfal
"""
#!pip install treelib
import copy
from experiment_design import random_matrix
from experiment_design import difficult_Orthogonal_experimental_design_judgement as Orthogonal_experimental_design_judgement
from experiment_design import check_difficult_Orthogonal_experimental_design_judgement
import random
import numpy as np

import time
from torch.autograd import Variable
import math


import torch
import torch.nn as nn
import torch.nn.functional as F
#from new_forward_backward_10_28_DNN_version import MCTS
import torch.optim as optim


class ReplayMemory(object):
    def __init__(self,x_list,next_board_list,reward_list,max_step=27*10, capacity=50000):
        self.max_step=max_step
        self.capacity = capacity
        self.x_list = list(x_list)
        self.next_board_list=list(next_board_list)
        self.reward_list=list(reward_list)
        self.is_av = False
        self.batch_size = 64
        self.max_score=0

    def remember(self, x, reward,next_board,max_reward=0.1):

        if (max(reward)>max_reward and max(reward)>0.1) or min(reward)==-1:
            
            self.x_list.append(x)
            self.reward_list.append(reward)
            self.next_board_list.append(next_board)
            
            if self.max_score<max(reward):

                self.max_score=max(reward)
#                print(self.max_score)
#                print(reward)
            if len(self.x_list)>64*self.max_step:
                i=0
                try:
                    while True:
                        if max(self.reward_list[i])!=self.max_score and min(self.reward_list[i])!=-1:
                            try:
                                del self.next_board_list[i]
                                del self.x_list[i]
                                del self.reward_list[i]
                                break
                            except:
                                pass
                        else:
                            i+=1
                except:
                    del self.next_board_list[0]
                    del self.x_list[0]
                    del self.reward_list[0]

    def sample(self,i):
#        order=i#random.randint(0,len(self.x_list)-self.batch_size-1)
        #print(random.sample(self.x_list, self.batch_size))
        
        return np.array(self.x_list[i*self.batch_size:(i+1)*self.batch_size]),np.array(self.reward_list[i*self.batch_size:(i+1)*self.batch_size]),np.array(self.next_board_list[i*self.batch_size:(i+1)*self.batch_size])

    def is_available(self):
        if len(self.x_list) > self.batch_size:
            self.is_av = True
        return self.is_av


class DQN(nn.Module):
    def __init__(self, n,s,n_action,del_column_num=5,batch_size=64):
        super(DQN, self).__init__()
        self.batch_size=batch_size
        self.n=n
        self.s=s
        self.conv1 = nn.Conv2d(1, 32, kernel_size=2, padding=1)
        self.conv2 = nn.Conv2d(32, 64, kernel_size=2, padding=1)
        self.conv3 = nn.Conv2d(64, 128, kernel_size=2, padding=1)
        self.fc0 = nn.Linear(128*(n+3)*(s+3), 512)
        self.fc1 = nn.Linear(512, n_action)
    def forward(self, state_input):
        if len(state_input.shape)==3:
            state_input=torch.unsqueeze(state_input, dim=1)
        x_act = F.relu(self.conv1(state_input))
        x_act = F.relu(self.conv2(x_act))
        x_act = F.relu(self.conv3(x_act))
        x_act = x_act.view(-1, 128*(self.s+3)*(self.n+3))
        
#        x_act = torch.cat(inputs=(state_input, action),1)
        
        x_act = F.relu(self.fc0(x_act))
        x_act = self.fc1(x_act)
        
        return x_act

class RL(object):
    def __init__(self, board,n,s,q,n_action,del_column_num=5,pred_DQN=None):
        self.board=board
        self.n=n
        self.s=s
        self.q=q
        self.n_action=n_action
        self.batch=64
        self.del_column_num=del_column_num
        if pred_DQN:
            self.pred_DQN = pred_DQN
        else:
            self.pred_DQN = DQN(n,s,n_action,del_column_num=del_column_num)
        self.loss = nn.MSELoss()
        self.l2_const = 5e-4
        if torch.cuda.is_available():
            self.pred_DQN = self.pred_DQN.cuda()
            self.loss=self.loss.cuda()
            print("Using GPU")
        else:
            print("Using CPU")
        self.target_DQN = copy.deepcopy(self.pred_DQN)
        self.optimizer = optim.Adam(self.pred_DQN.parameters(), weight_decay=self.l2_const)
        
    def training(self,im,label,next_im_list,gamma=0.5):
        self.pred_DQN.train()
        im=torch.from_numpy(im)
        im=im.float()
        im = Variable(im)
        
        target_out=[]
        next_im_list=torch.from_numpy(next_im_list)
        for i in range(self.n_action):
            next_im=next_im_list[:,i,:,:]
            next_im=next_im.float()
            if torch.cuda.is_available():
                next_im=next_im.cuda()

            result=torch.max(self.target_DQN(next_im),1)[0]
            result=result.reshape((len(result),1))
            if i==0:
                target_out=result
            else:
                target_out=torch.cat((target_out,result),-1)


        if torch.cuda.is_available():
            im=im.cuda()

        
        label=torch.from_numpy(label)
        label=label.float()
        label = Variable(label)
        if torch.cuda.is_available():
            label=label.cuda()
            target_out=target_out.cuda()
        target_out = Variable(target_out)
        #im=torch.tensor(im,dtype=torch.float32)
        # 前向传播
        pred_out = self.pred_DQN(im)
        true_label=label+gamma*target_out

        true_label = Variable(true_label)
        loss = self.loss(pred_out, true_label)
        action=pred_out.argmax(dim=1)
        true_action=true_label.argmax(dim=1)
#        print(pred_out,true_label)
#        print(action,true_action)
        acc = (action == true_action).sum().item()
        # 反向传播
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()
        return loss,acc
    def get_action(self,random_percent=0,success_action=None):
        if success_action==None:
            success_action=list(range(self.q))
        available_action=self.board.available_action
        if random.random()>random_percent:
            x=torch.from_numpy(self.board.matrix)
            x=x.float()
            x=Variable(x)
            x=x.reshape((1,1,x.shape[0],x.shape[1]))
            
            if torch.cuda.is_available():
                x=x.cuda()
            value=self.pred_DQN(x)
            action=value.argmax()
            if action not in success_action:
                action = random.choice(success_action)
            return available_action[action]
        else:
            action_index = random.choice(success_action)
#            action_index=random.randint(0,self.n_action-1)
#            act=np.zeros(self.n_action)
#            act[action_index]=1
            return available_action[action_index]
        
        
        
class Board(object):
    def __init__(self, n=27,s=4,q=3,simulation_ratio=1,UCB_ratio=0.5,sub_matrix=None):
        self.n = n
        self.s = s
        self.q = q
        self.repeat=int(n/q)
        self.sub_repeat=q#int(self.repeat/q)
        self.C_q_2=math.factorial(q)/math.factorial(q-2)/math.factorial(2)
        #self.each_column_action_len=int(self.C_q_2*self.repeat**2)
        self.each_column_action_len=27
        self.simulation_ratio=simulation_ratio
        self.UCB_ratio=UCB_ratio
        

        
        self.states = {} # board states, key:move, value: piece type
#    def init_board(self):
        if not type(sub_matrix)==type(np.array([1])):
            self.matrix=random_matrix(self.n,self.s,self.q)
            self.fix_column_num=2
        else:
            self.matrix=random_matrix(self.n,self.s,self.q,sub_matrix=sub_matrix)
            self.fix_column_num=np.shape(sub_matrix)[1]+1
        #init available action
        self.available_action=list(range(0,q))
        
    def action_to_matrix(self, action,update_place):
        """
        input is like [(1,5),(2,5)]
        """
        self.matrix[update_place[0],update_place[1]]=action
    def update(self,action,update_place):
        self.action_to_matrix(action,update_place)

def count_sucessful_steps(matrix,first_column=5,q=3):
    count=0
    n,s=np.shape(matrix)
    for c in range(first_column,s):
        for i in range(n):
            if matrix[i,c]==-1:
                return count/(n*(s-first_column))
            else:
                count+=1
    return count/(n*(s-first_column))

def gen_data_from_training(matrix,first_column=3,q=3):
    data=[]
    score=count_sucessful_steps(matrix,first_column=first_column)

    def get_im(matrix,c,i):
        im =copy.deepcopy(matrix)
        for h in range(c+1,s):
            for j in range(n):
                im[-j-1,h]=-1
        for j in range(n-i):
            im[-j-1,c]=-1
        return im
    
    for c in range(first_column,s):
        for i in range(n):
            
            if matrix[i,c]==-1:
                if matrix[-1,-1]==-1:
                    data[-1][1]=[0]*q
                    data[-1][1][matrix[i-1,c]]=-1
                return data
            im = get_im(matrix,c,i)
            label = [0]*q
            if matrix[-1,-1]==-1:
                label[matrix[i,c]]=score
            else:
                label[matrix[i,c]]=1
            next_board_list=[]
            for j in range(q):
                im_copy = copy.deepcopy(im)
                im_copy[i,c]=j
                next_board_list.append(im_copy)
            data_list=[im,label,next_board_list]
            data.append(data_list)

    return data
if __name__ == '__main__':
    n=18
    s=7
    q=3
    check_k=4
    max_step=n*(s-2)
    min_length=36
    
    fail_data_set=[]
    from SC import sc
    import scipy.io as sio
    import os
    path = os.path.join(os.getcwd(), 'L18_lb_coll.mat')
    lb_coll_data = sio.loadmat(path)
    lb_coll = lb_coll_data['lb_coll']
    
#    a=np.array([[0,0,0,0,0],[0,0,1,0,0],[0,0,2,0,0],[0,1,0,1,1],[0,1,1,1,1],[0,1,2,1,1],[0,2,0,2,2],[0,2,1,2,2],[0,2,2,2,2],[1,0,0,1,2],[1,0,1,1,2],[1,0,2,1,2],[1,1,0,2,0],[1,1,1,2,0],[1,1,2,2,0],[1,2,0,0,1],[1,2,1,0,1],[1,2,2,0,1],[2,0,0,2,1],[2,0,1,2,1],[2,0,2,2,1],[2,1,0,0,2],[2,1,1,0,2],[2,1,2,0,2],[2,2,0,1,0],[2,2,1,1,0],[2,2,2,1,0]])
#    copy_a=copy.deepcopy(a[:,2])
#    a[:,2], a[:,3], a[:,4] = a[:,3], a[:,4],copy_a
#    true = copy.deepcopy(a)
#    true=a[:,:3]
    true=None

    board=Board(n=n,s=s,q=q,simulation_ratio=10,UCB_ratio=1,sub_matrix=true)
    if not type(true)==type(np.array([1])):
        column=2
        number_steps = (s-column)*n
    else:
        _,column=np.shape(true)
        
        number_steps = (s-column)*n
    
    
    update_place=[0,column]
    
    

    
    n_action=q

    '''
    直接调用训练好的模型
    '''
    model=torch.load('brain.pred_DQN_L18_2 1 0 0 2.pkl')
    random_percent_list=[0]
    open_train=0
    '''
    开始训练
    '''
#    model=None
#    open_train=1
#    random_percent_list=[0.9,0.7,0.5,0.3,0.1]
    
    
    
    x_list=[]#np.load("memory_x_list.npy")
    reward_list=[]#np.load("memory_reward_list.npy")
    next_board_list=[]#np.load("next_board_list.npy")
    
    brain = RL(board,n,s,q,n_action,pred_DQN=model)
    memory = ReplayMemory(x_list,next_board_list,reward_list,max_step=max_step)
    
     
    
    max_length=0
    last_max=0
    count=0
    last_count=0
    
    init_matrix = copy.deepcopy(brain.board.matrix)
    

    update_place=[0,column]
    
#    print(brain.board.matrix)
    #######
    success=0
    begin_time=time.time()
    for t in range(10000):
      
        if t>=100:
            random_percent_list=[0.5,0.3,0.1,0.01,1/t]
        elif t>500:
            random_percent_list=[0.1,0.01,1/t]
        
        print('t',t,random_percent_list[t%len(random_percent_list)])
        for MC_iter in range(10*number_steps):
            
            
            success_action = []
            for i in range(q):
                test_board=copy.deepcopy(brain.board.matrix)
                test_board[update_place[0],update_place[1]]=i
                if Orthogonal_experimental_design_judgement(test_board,update_place,q)==0:

                    for j in range(q):
                        copy_update_place = [(update_place[0]+1)%n,update_place[1]+int((update_place[0]+1)/n)]
                        next_test_board=copy.deepcopy(test_board)
                        next_test_board[copy_update_place[0],copy_update_place[1]]=j
                        if Orthogonal_experimental_design_judgement(next_test_board,copy_update_place,q)==0:
                            for k in range(q):
                                k_copy_update_place = [(copy_update_place[0]+1)%n,copy_update_place[1]+int((copy_update_place[0]+1)/n)]
                                k_next_test_board=copy.deepcopy(next_test_board)
                                k_next_test_board[k_copy_update_place[0],k_copy_update_place[1]]=k
                                if Orthogonal_experimental_design_judgement(k_next_test_board,k_copy_update_place,q)==0:
                                    success_action.append(i)
                                    break
                        if i in success_action:
                            break
            
            
            
            for i in range(q):
                test_board=copy.deepcopy(brain.board.matrix)
                test_board[update_place[0],update_place[1]]=i
                if Orthogonal_experimental_design_judgement(test_board,update_place,q)==0:
                    success_action.append(i)
#            
            
            
            
            
            reward=0
            action= brain.get_action(random_percent=random_percent_list[t%len(random_percent_list)],success_action=success_action)
            next_board_list=[]
            reward_list=[]
            ########################
            brain.board.update(action,update_place)

            ##计算loss -> 也就是reward的相反数
            loss = Orthogonal_experimental_design_judgement(brain.board.matrix,update_place,q)
            #####################################################################

            ##判断是否胜利，并计算reward
            if loss==0 and update_place[0]==n-1 and update_place[1]==s-1:
                    
                    print("success when find the reward")
                    print(brain.board.matrix)
                    torch.save(brain.pred_DQN, 'brain.pred_DQN.pkl')
                    print("hhhhhhhhhhhhhhhhhhhhhhhhhhhhh")
                    success=1
                    break
            elif loss==0:
                last_update = update_place
                last_max_length=max_length
                update_place = [(update_place[0]+1)%n,update_place[1]+int((update_place[0]+1)/n)]
                max_length = max(max_length,update_place[0]+(update_place[1]-column)*n)
                success_action = list(range(q))
                success_action=[]
                for i in range(q):
                    test_board=copy.deepcopy(brain.board.matrix)
                    test_board[update_place[0],update_place[1]]=i
                    if Orthogonal_experimental_design_judgement(test_board,update_place,q)==0:
                        success_action.append(i)
#                for i in range(q):
#                    test_board=copy.deepcopy(brain.board.matrix)
#                    test_board[update_place[0],update_place[1]]=i
#                    if Orthogonal_experimental_design_judgement(test_board,update_place,q)==0:
#                        
#                        for j in range(q):
#                            copy_update_place = [(update_place[0]+1)%n,update_place[1]+int((update_place[0]+1)/n)]
#                            next_test_board=copy.deepcopy(test_board)
#                            next_test_board[copy_update_place[0],copy_update_place[1]]=j
#                            if Orthogonal_experimental_design_judgement(next_test_board,copy_update_place,q)==0:
#                                for k in range(q):
#                                    k_copy_update_place = [(copy_update_place[0]+1)%n,copy_update_place[1]+int((copy_update_place[0]+1)/n)]
#                                    k_next_test_board=copy.deepcopy(next_test_board)
#                                    k_next_test_board[k_copy_update_place[0],k_copy_update_place[1]]=k
#                                    if Orthogonal_experimental_design_judgement(k_next_test_board,k_copy_update_place,q)==0:
#                                        success_action.append(i)
#                                        break
#                            if i in success_action:
#                                break
                #Failed
#                print(success_action)
                if success_action==[]:
                    update_place=last_update
                    max_length = last_max_length
                    if update_place[0]+(update_place[1]-column)*n>=max_length and update_place[0]+(update_place[1]-column)*n>min_length:
                        print(brain.board.matrix)
                        print('data_len',len(memory.x_list))
                    if update_place[0]+(update_place[1]-column)*n>max_length-3 and update_place[0]+(update_place[1]-column)*n>min_length:
                        count+=1
                    if open_train and update_place[0]+(update_place[1]-column)*n>max_length-3 and update_place[0]+(update_place[1]-column)*n>min_length:
                        train_data=gen_data_from_training(brain.board.matrix,first_column=column,q=q)
                        for data_list in train_data:
                            memory.remember(data_list[0],data_list[1],data_list[2],max_reward=(max_length-3)/number_steps)
                    brain.board.matrix=copy.deepcopy(init_matrix) 
                    update_place=[0,column]
#                    max_length = max(max_length,update_place[0]+(update_place[1]-column)*n)
                else:#success
                    #If add a new column
#                    print('in',last_update[0],last_update[1])
                    if last_update[0]==n-1 and last_update[1]>=check_k-1:
                        check_board=copy.deepcopy(brain.board.matrix)[:,:last_update[1]+1]
                        n_cd, con, lb_coll, cc_coll =sc(n, s, q, check_board, lb_coll)
                        cc_coll=np.array(cc_coll,dtype=int)

                        if con in [1,2]:
                            print('success when find the reward')
                            print(np.concatenate((check_board,cc_coll),axis=1))
                            torch.save(brain.pred_DQN, 'brain.pred_DQN.pkl')
                            print('hhhhhhhhhhhhhhhhhhhhhhhhhhhhh')
                            success=1
                            break
                        elif con in [3,4]:
#                            print('dead')
                            fail_data_set.append(check_board)
                            update_place=last_update
                            max_length = last_max_length
                            if update_place[0]+(update_place[1]-column)*n>=max_length and update_place[0]+(update_place[1]-column)*n>min_length:
                                print(brain.board.matrix)
                                print('data_len',len(memory.x_list))
                            if update_place[0]+(update_place[1]-column)*n>max_length-3 and update_place[0]+(update_place[1]-column)*n>min_length:
                                count+=1
                            if open_train and update_place[0]+(update_place[1]-column)*n>max_length-3 and update_place[0]+(update_place[1]-column)*n>min_length:
                                train_data=gen_data_from_training(brain.board.matrix,first_column=column,q=q)
                                for data_list in train_data:
                                    memory.remember(data_list[0],data_list[1],data_list[2],max_reward=(max_length-3)/number_steps)
                            brain.board.matrix=copy.deepcopy(init_matrix) 
                            update_place=[0,column]
                        else:
                            pass
                            
            else:
                if update_place[0]+(update_place[1]-column)*n>=max_length:
                    print(brain.board.matrix)
                    print('data_len',len(memory.x_list))
                if update_place[0]+(update_place[1]-column)*n>max_length-3 and update_place[0]+(update_place[1]-column)*n>min_length:
                    count+=1
                if open_train:
                    
                    train_data=gen_data_from_training(brain.board.matrix,first_column=column,q=q)
                    for data_list in train_data:
                        memory.remember(data_list[0],data_list[1],data_list[2],max_reward=(max_length-3)/number_steps)
                brain.board.matrix=copy.deepcopy(init_matrix) 
                update_place=[0,column]
                
#            if MC_iter%10000==0 and t%10==0:
#                print("epoch",MC_iter)
#                print("count",count)
#                print("RL action",action)
#                print("RL max_length",max_length)
#                try:
#                    print('loss',loss.item())
#                except:
#                    pass
#                print("********************")

            if MC_iter%10000==0 and t%100==0:
                print("save the model and data")
                torch.save(brain.pred_DQN, 'brain.pred_DQN.pkl')
                np.save("memory_x_list",memory.x_list)
                np.save("memory_reward_list",memory.reward_list)
                np.save("next_board_list",memory.next_board_list)
                print('finished')

        if success:
            break
        if open_train:
            if last_count<count:
                print("max_length",max_length)
                print("count",count)
            for epoch in range(1,13):
                acc=0 
                for j in range(int(len(memory.x_list)/memory.batch_size)):
                    
                    x,y,z=memory.sample(j)
                    loss,acc_add=brain.training(x,y,z)
                    acc+=acc_add
                try:
                    if epoch%6==0:
                        brain.target_DQN.load_state_dict(brain.pred_DQN.state_dict())
                except:
                    pass
        last_count = count
    final_time = time.time()-begin_time
    print('time:',final_time)
    
    np.save('fail_data_set',fail_data_set)
    results=np.concatenate((check_board,cc_coll),axis=1)

    cd=check_difficult_Orthogonal_experimental_design_judgement(results)
    if cd==0:
        print('the matrix is OD')
